package com.aptitest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WtOnlineTestBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(WtOnlineTestBackendApplication.class, args);
	}
}
