package com.aptitest.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity
@Table(name = "users")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long userId;
	
	@NotNull
	private String name;
	
	@Column(unique=true)
	@NotNull
	private String email;
	
	@Column(unique=true)
	@NotNull
	private String username;
	
	@Column(unique=true)
	private String mobile;
	
	@NotNull
	private String password;
	
	@NotNull
	private Boolean isActivated;
	
	@NotNull
	private Boolean isDeleted;
}
