package com.aptitest.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity
@Table(name = "results")
public class Result {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long resultId;
	
	@NotNull
	private Long participantId;
	
	@NotNull
	private Long testId;
	
	@NotNull
	private Integer marks;
	
	@NotNull
	private Integer totalMarks;
}
