package com.aptitest.models;

import java.util.ArrayList;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity
@Table(name = "tests")
public class Test {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long testId;
	
	@NotNull
	private String duration;
	
	@NotNull
	private Date startDate;
	
	@NotNull
	private Date endDate;
	
	@NotNull
	private String accessKey;
	
	@NotNull
	private ArrayList<Long> questionSet;

	@NotNull
	private Boolean negativeMarking;
	
	@NotNull
	private Long creatorId;
	
}
