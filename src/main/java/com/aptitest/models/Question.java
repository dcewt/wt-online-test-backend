package com.aptitest.models;

import java.util.ArrayList;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity
@Table(name = "questions")
public class Question{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long quesId;
	
	@NotNull
	private String question;
	
	@NotNull
	private ArrayList<String> choices;
	
	@NotNull
	private String answer;
	
}
