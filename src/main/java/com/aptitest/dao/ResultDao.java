package com.aptitest.dao;

import java.util.ArrayList;

import org.springframework.data.repository.CrudRepository;

import com.aptitest.models.Result;

public interface ResultDao extends CrudRepository<Result, Long>{

	public Result findByTestIdAndParticipantId(Long testId, Long participantId);

	public ArrayList<Result> findByTestId(Long testId);
	
	public Result save(Result result);

}
