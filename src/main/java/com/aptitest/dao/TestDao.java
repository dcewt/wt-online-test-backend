package com.aptitest.dao;

import org.springframework.data.repository.CrudRepository;

import com.aptitest.models.Test;

public interface TestDao extends CrudRepository<Test, Long>  {
	public Test save(Test test);
	
	public Test findByTestIdAndAccessKey(Long testId, String accessKey);

	public Test findByTestId(Long testId);
	
}
