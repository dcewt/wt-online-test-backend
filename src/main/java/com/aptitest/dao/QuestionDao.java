package com.aptitest.dao;

import org.springframework.data.repository.CrudRepository;

import com.aptitest.models.Question;

public interface QuestionDao extends CrudRepository<Question, Long> {
	
	public Question save(Question ques);
	
	public Question findByQuesId(Long quesId);
}
