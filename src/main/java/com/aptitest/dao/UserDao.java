package com.aptitest.dao;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import com.aptitest.models.User;

@Transactional
public interface UserDao extends CrudRepository<User, Long> {
	
	public User save(User user);
	
	public User findByUsername(String username);
	
	public User findByEmail(String email);
	
	public User findByMobile(String mobile);

	public User getUserByUserId(Long id);

	public User getByUserId(Long id);

}
