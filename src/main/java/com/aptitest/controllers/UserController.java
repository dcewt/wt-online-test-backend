package com.aptitest.controllers;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.aptitest.dao.UserDao;
import com.aptitest.models.User;

@RestController
@RequestMapping(value="/user")
public class UserController {
	
	@Autowired
	private UserDao userDao;
	
	@RequestMapping(value="/ping",method=RequestMethod.GET)
	public String ping(){
		return "pong";
	}
	
	@RequestMapping(value="/activate/{id}",method=RequestMethod.GET,consumes="application/json")
	public User activateUser(@PathVariable Long id){
		User user = null;
		try{
			user = userDao.getUserByUserId(id);
			System.out.println(user);
			user.setIsActivated(true);
			user = userDao.save(user);
		}
		catch(Exception e){
			e.getMessage();
		}
		return user;
	}
	
	@RequestMapping(value="/delete/{id}",method=RequestMethod.GET,consumes="application/json")
	public User deleteUser(@PathVariable Long id){
		User user = null;
		try{
			user = userDao.getUserByUserId(id);
			user.setIsDeleted(true);
			user = userDao.save(user);
		}
		catch(Exception e){
			e.getMessage();
		}
		return user;
	}
	
	@RequestMapping(value="/reactivate/{id}",method=RequestMethod.GET,consumes="application/json")
	public User reactivateUser(@PathVariable Long id){
		User user = null;
		try{
			user = userDao.getUserByUserId(id);
			user.setIsDeleted(false);
			user = userDao.save(user);
		}
		catch(Exception e){
			e.getMessage();
		}
		return user;
	}
	
	@RequestMapping(value="/signup",method=RequestMethod.POST,consumes="application/json")
	public User saveUser(@RequestBody User user){
		User user2 = null;
		try{
			user2 = userDao.save(user);
		}
		catch(Exception e){
			System.out.println(e.getMessage());
		}
		return user2;
	}
	
	@RequestMapping(value="/get/{id}",method=RequestMethod.GET,consumes="application/json")
	public User getUser(@PathVariable Long id){
		User user = null;
		try{
			user = userDao.getByUserId(id);
		}
		catch(Exception e){
			e.getMessage();
		}
		return user;
	}
	
	@RequestMapping(value="/login",method=RequestMethod.POST,consumes="application/json")
	public User getUserDetails(@RequestBody User user){
		if(StringUtils.isNotBlank(user.getUsername())){
			User user2 = userDao.findByUsername(user.getUsername());
			//System.out.println(user.getUsername());
			System.out.println(user2);
			return user2;
		}
		else if(StringUtils.isNotBlank(user.getEmail())){
			User user2 = userDao.findByEmail(user.getEmail());
			return user2;
		}
		else if(StringUtils.isNotBlank(user.getMobile())){
			User user2 = userDao.findByMobile(user.getMobile());
			return user2;
		}
		return null;
	}
	
}