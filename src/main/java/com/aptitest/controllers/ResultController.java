package com.aptitest.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.aptitest.dao.ResultDao;
import com.aptitest.models.Result;
import com.aptitest.models.Test;

@RestController
@RequestMapping("/result")
public class ResultController {
	@Autowired
	private ResultDao resultDao;
	
	@RequestMapping(value="/ping",method=RequestMethod.GET)
	public String ping(){
		return "pong";
	}
	
	@RequestMapping(value="/save",method=RequestMethod.POST)
	public Result saveResult(@RequestBody Result result){
		Result result2 = null;
		try{
			result2 = resultDao.save(result);
		}
		catch(Exception e){
			e.getMessage();
		}
		return result2;
	}
	
	@RequestMapping(value="/getResult",method=RequestMethod.GET)
	public Result getResult(@RequestParam("testId") Long testId, @RequestParam("participantId") Long participantId){
		Result result = null;
		try{
			result = resultDao.findByTestIdAndParticipantId(testId, participantId);
		}
		catch(Exception e){
			e.getMessage();
		}
		return result;
	}
	
	@RequestMapping(value="/getResults",method=RequestMethod.GET)
	public ArrayList<Result> getResults(@RequestParam("testId") Long testId){
		ArrayList<Result> results = null;
		try{
			results = resultDao.findByTestId(testId);
		}
		catch(Exception e){
			e.getMessage();
		}
		return results;
	}
}
