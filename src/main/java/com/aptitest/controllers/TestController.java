package com.aptitest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.aptitest.dao.TestDao;
import com.aptitest.models.Test;

@RestController
@RequestMapping("/test")
public class TestController {
	@Autowired
	private TestDao testDao;
	
	@RequestMapping(value="/ping",method=RequestMethod.GET)
	public String ping(){
		return "pong";
	}
	
	@RequestMapping(value="/create",method=RequestMethod.POST)
	public Test createTest(@RequestBody Test test){
		Test test2 = null;
		try{
			test2 = testDao.save(test);
		}
		catch(Exception e){
			e.getMessage();
		}
		return test2;
	}
	
	/*@RequestMapping(value="/get",method=RequestMethod.GET)
	public Test getTest(@RequestParam("testId") Long testId, @RequestParam("accessKey") String accessKey){
		Test test = testDao.findByTestIdAndAccessKey(testId, accessKey);
		return test;
	}*/
	
	@RequestMapping(value="/get",method=RequestMethod.GET)
	public Test getTest(@RequestParam("testId") Long testId){
		Test test = null;
		try{
			test = testDao.findByTestId(testId);
		}
		catch(Exception e){
			e.getMessage();
		}
		return test;
	}
	
}
