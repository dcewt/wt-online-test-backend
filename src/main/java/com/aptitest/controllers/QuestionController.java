package com.aptitest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.aptitest.dao.QuestionDao;
import com.aptitest.models.Question;

@RestController
@RequestMapping("/question")
public class QuestionController {

	@Autowired
	private QuestionDao quesDao;

	@RequestMapping(value="/ping",method=RequestMethod.GET)
	public String ping(){
		return "pong";
	}
	
	@RequestMapping(value="/save",method=RequestMethod.POST)
	public Long saveQuestion(@RequestBody Question ques){
		Question ques2 = null;
		try{
			ques2 = quesDao.save(ques);
		}
		catch(Exception e){
			e.getMessage();
		}
		if(ques2 != null)
			return ques2.getQuesId();
		else
			return null;
	}
	
	@RequestMapping(value="/get",method=RequestMethod.GET)
	public Question getQuestion(@RequestParam("id") Long id){
		Question q = null;
		try{
			q = quesDao.findByQuesId(id);
		}
		catch(Exception e){
			e.getMessage();
		}
		return q;
			
	}
}
